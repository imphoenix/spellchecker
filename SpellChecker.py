#####
# Spell Checker Program created in Python
#
# Implemented Using TRIE data structures
# The Dictionary is Represented using TRIE
#   which implements inserting words into the TRIE
#   it also implements SPELL corrections suggestions
#
# @author: Surender Yerva
#####

import operator
class Trie():
    ''' Trie Data Structure used for representing the Dictionary'''
    def __init__(self):
        self.D = dict()
        self.word = ""
        return

    def insert(self,word,i=0):
        '''Insert words into the Trie DS'''
        word=word.lower()
        if i==len(word):
            self.word=word
            return
        if not self.D.has_key(word[i]):
            self.D[word[i]] = Trie()
        self.D[word[i]].insert(word,i+1)
        return

    def toString(self,pre="#"):
        '''String representation of the TRIE'''
        ans=""
        ans = ans+":{"+self.word+"}\n" if len(self.word)!=0 else "\n"
        for k in self.D.keys():
            ans = ans+pre+k+self.D[k].toString(pre+"->")
        return ans

    def wordCorrections(self,word,ERR=3):
        '''Suggests 5 closest alternatives for WORD in the TRIE'''
        word=word.lower()
        ANS = dict()
        self._suggestions(word,0,ERR,0,"",ANS)
        sorted_ANS = sorted(ANS.iteritems(), key=operator.itemgetter(1))
        return sorted_ANS[0:min(len(sorted_ANS),5)]
    
    def _suggestions(self,word,i,MAX_ERR,err,sofar,ANS):
        if err>=MAX_ERR: return
        if len(self.word)>0 and len(word)-i+err<MAX_ERR:
            ANS[sofar] = min(ANS[sofar],len(word)-i+err) if ANS.has_key(sofar) else len(word)-i+err
        for k in self.D.keys():
            if i<len(word):
                if k==word[i]:
                    self.D[k]._suggestions(word,i+1,MAX_ERR,err,sofar+k,ANS) ##foundMatchSoFar
                else:
                    self.D[k]._suggestions(word,i+1,MAX_ERR,err+1,sofar+k,ANS) ##Transformation
                    self.D[k]._suggestions(word,i,MAX_ERR,err+1,sofar+k,ANS) ##Insertion
            self._suggestions(word,i+1,MAX_ERR,err+1,sofar,ANS) ##Deletion
        return

def makeTrieDict(file):
    T = Trie()
    for l in open(file):
        T.insert(l.strip().lower())
    return T

###### Simple TEST ##########
##T=Trie()
##T.insert("The",0)
##T.insert("Them",0)
##T.insert("This",0)
##T.insert("apple",0)
##print T.toString('@')
##ANS=T.wordCorrections("Tha",6)
##print ANS

T=makeTrieDict('data/words')
print 'Trie of Dictonary Words Created'

while True:
    word=raw_input("Input > ").strip()
    if word=="exit" : break
    ANS = ["\t"+x[0]+"\t: "+str(x[1]) for x in T.wordCorrections(word)]
    print "\n".join(ANS)
    

